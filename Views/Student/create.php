
<html>
<head>
    <title>Submitting Student Courses</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Enter your details</h2>
    <form role="form" method="post" action="store.php">
        <label>Enter your name :</label> <br>
        <input type="text" name="name" placeholder="Enter your name"><br><br>
        <label>Select your courses:</label><br>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="PHP">PHP</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="JAVA">JAVA</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="PYTHON" >PYTHON</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="HTML" >HTML</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="ORACLE" >ORACLE</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="DOT NET" >DOT NET</label>
        </div>
        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>


