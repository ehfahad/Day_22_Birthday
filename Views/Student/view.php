<?php

include_once('../../vendor/autoload.php');
use App\Student\Student;





$obj=new Student();
$obj->prepare($_GET);
$single= $obj->view();

?>



<html>
<head>
    <title>View Student Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>View Student details</h2>

        <label>Student name :</label> <br>
        <input type="text" name="name" value="<?php echo $single['name'] ?>"><br><br>
        <label>Selected courses:</label><br>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="PHP" disabled <?php if(in_array("PHP",$single['course'])): ?> checked <?php endif ?>>PHP</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="JAVA" disabled <?php if(in_array("JAVA",$single['course'])): ?> checked <?php endif ?>>JAVA</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="PYTHON" disabled <?php if(in_array("PYTHON",$single['course'])): ?> checked <?php endif ?>>PYTHON</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="HTML" disabled <?php if(in_array("HTML",$single['course'])): ?> checked <?php endif ?> >HTML</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="ORACLE" disabled <?php if(in_array("ORACLE",$single['course'])): ?> checked <?php endif ?>>ORACLE</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="DOT NET" disabled <?php if(in_array("DOT NET",$single['course'])): ?> checked <?php endif ?> >DOT NET</label>
        </div>
        <a href="index.php" class="btn btn-primary">Done</a>

</div>

</body>
</html>



