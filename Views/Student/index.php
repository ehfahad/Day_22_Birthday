<?php
session_start();

include_once('../../vendor/autoload.php');
use App\Student\Student;
use App\Message\Message;
use App\Utility\Utility;

$obj= new Student();
$allCourse= $obj->index();

?>
<html>
<head>
    <title>Student Informations</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Manage Student Details</h2>
   <?php if(array_key_exists("message",$_SESSION) && !empty($_SESSION['message'])): ?>
    <div id="message" class="alert alert-info">
       <center> <?php echo Message::message() ?></center>
    </div>
    <?php endif; ?>
    <a href="create.php" class="btn btn-primary" role="button">Create Again</a>
    <a href="trashlist.php" class="btn btn-primary" role="button">View Trashlist</a>
    <table class="table">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Selected Courses</td>
                <td>Action</td>
            </tr>
        </thead>

        <tbody>
        <?php foreach($allCourse as $course){ ?>
            <tr>
                <td><?php echo $course->id?></td>
                <td><?php echo $course->fullName?></td>
                <td><?php echo $course->courseName?></td>
                <td>
                    <a href="create.php" class="btn btn-primary" role="button">Create</a>
                    <a href="view.php?id=<?php echo $course->id ?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $course->id ?>" class="btn btn-primary" role="button">Update</a>
                    <a href="trash.php?id=<?php echo $course->id ?>" class="btn btn-warning" role="button">Trash</a>
                    <a href="delete.php?id=<?php echo $course->id ?>" class="btn btn-danger" onclick="return ConfirmDelete()" role="button">Delete</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

    <script>
        $('#message').show().delay(1500).fadeOut();

        function ConfirmDelete(){
            var x=confirm("Sure to delete?");
            if(x)
            return true;
            else
            return false;
        }
    </script>
</body>
</html>


