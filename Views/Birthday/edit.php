<?php

include_once('../../vendor/autoload.php');
use App\Birthday\Birthday;
use App\Utility\Utility;



$obj=new Birthday();
$obj->prepare($_GET);
$single = $obj->view();
//Utility::debug($single);
?>




<html>
<head>
    <title>Submitting Student Courses</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Update your details</h2>
    <form role="form" method="post" action="update.php">
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
        <label>Update your name :</label> <br>
        <input type="text" name="name" placeholder="Enter your name" value="<?php echo $single->name ?>"><br><br>
        <label>Update your birthday :</label><br>
        <div class="checkbox">
            <label><input type="date" name="date" value="<?php echo $single->date ?>"> </label>
        </div>

        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>


