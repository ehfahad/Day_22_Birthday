<?php

namespace App\Student;

use App\Message\Message;
use App\Utility\Utility;

class Student{
    public $id='';
    public $course='';
    public $name='';
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","labxm6b22");
    }


    public function prepare($data=''){
        if(array_key_exists("id",$data))
            $this->id=$data['id'];

        if(array_key_exists("course",$data))
            $this->course=$data['course'];

        if(array_key_exists("name",$data))
            $this->name=$data['name'];
    }

    public function index(){
        $allCourse=array();
        $query = "SELECT * FROM  `student_information` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result))
            $allCourse[]=$row;

        return $allCourse;
    }


    public function store(){
        $query = "INSERT INTO `labxm6b22`.`student_information` (`fullName`, `courseName`) VALUES ('".$this->name."', '".$this->course."')";
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";

    }


    public function view(){
        $single=array();
        $query = "SELECT * FROM  `student_information` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        //Utility::dd($result);
        if($row=mysqli_fetch_assoc($result)){
            $single['name']=$row['fullName'];
            $single['course']=explode(",",$row['courseName']);
        }
        return $single;
    }


    public function update(){
        $query = "UPDATE `labxm6b22`.`student_information` SET `fullName` = '".$this->name."', `courseName` = '".$this->course."' WHERE `student_information`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }

    public function delete(){
        $query = "DELETE FROM `labxm6b22`.`student_information` WHERE `student_information`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been deleted successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }

    public function trash(){
        $query = "UPDATE `labxm6b22`.`student_information` SET `deleted_at` = ".time()." WHERE `student_information`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been sent to trash successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }

    public function trashed(){
        $allCourse=array();
        $query = "SELECT * FROM  `student_information` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result))
            $allCourse[]=$row;

        return $allCourse;
    }


    public function recover(){
        $query = "UPDATE `labxm6b22`.`student_information` SET `deleted_at` = NULL WHERE `student_information`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been recovered successfully");
            Utility::redirect("trashlist.php");
        }
        else
            echo "ERROR";
    }


    public function recoverSelected($IDs=array()){
    if(count($IDs>0)){
        $ids= implode(",",$IDs);
        ///Utility::dd($ids);
        $query = "UPDATE `labxm6b22`.`student_information` SET `deleted_at` = NULL WHERE `student_information`.`id` IN (".$ids.")";
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been recovered successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }

}

    public function deleteSelected($IDs=array()){
        if(count($IDs>0)){
            $ids= implode(",",$IDs);

            $query = "DELETE FROM `labxm6b22`.`student_information` WHERE `student_information`.`id` IN (".$ids.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR";
        }

    }
}